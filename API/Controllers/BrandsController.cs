﻿using API.Dtos;
using API.Errors;
using API.Helpers;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BrandsController:BaseApiController
    {
        private readonly IBrandRepository _brandRepository;
        private readonly IUnitOfWork _unitOfWork;

        public BrandsController(IBrandRepository brandRepository, IUnitOfWork unitOfWork)
        {
            _brandRepository = brandRepository;
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<ActionResult<Pagination<ProductBrand>>> Get([FromQuery] BrandSpecParams brandParams)
        {
            var spec = new BrandsWithNameSpecification(brandParams);

            var countSpec = new BrandsForCountSpecification(brandParams);

            var totalItems = await _unitOfWork.Repository<ProductBrand>().CountAsync(countSpec);

            var brands = await _unitOfWork.Repository<ProductBrand>().ListAsync(spec);

            return Ok(new Pagination<ProductBrand>(brandParams.PageIndex, brandParams.PageSize, totalItems, brands));
        }

        [HttpGet("names/{name}")]
        public async Task<ActionResult> BrandNameExist(string name)
        {
            var brands = await _brandRepository.GetBrandsAsync();

            var exist= brands.Select(n => n.Name).Any(n => n == name);

            return Ok(exist);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get([FromRoute] int id)
        {
            var brand = await _brandRepository.GetBrandByIdAsync(id);

            if (brand==null)
            {
                return BadRequest();
            }

            return Ok(brand);
        }

        [HttpPost]
        public async Task<ActionResult<ProductBrand>> Post([FromBody] ProductBrandDto productBrand)
        {
            var brand = new ProductBrand { Name = productBrand.Name };

            var newBrand=_unitOfWork.Repository<ProductBrand>().Add<ProductBrand>(brand);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem creating brand"));

            return newBrand;
        }

        [HttpPut]
        public async Task<ActionResult<ProductBrand>> Put([FromBody] ProductBrand productBrand)
        {
            var brand = new ProductBrand { Name = productBrand.Name, Id=productBrand.Id };

            _unitOfWork.Repository<ProductBrand>().Update(brand);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem updating brand"));

            return productBrand;
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var brand = await _unitOfWork.Repository<ProductBrand>().GetByIdAsync(id);

            _unitOfWork.Repository<ProductBrand>().Delete(brand);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem deleting brand"));

            return Ok();
        }
    }
}
