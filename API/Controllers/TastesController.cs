﻿using API.Dtos;
using API.Errors;
using API.Helpers;
using AutoMapper;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{

    public class TastesController : BaseApiController
    {
        private readonly ITasteRepository _tasteRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TastesController(ITasteRepository tasteRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _tasteRepository = tasteRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        // GET: api/<TastesController>
        [HttpGet]
        public async Task<ActionResult<Pagination<Taste>>> Get([FromQuery] TasteSpecParams specParams)
        {
            var spec = new TastesWithNamesSpecification(specParams);

            var countSpec = new TastesForCountSpecification(specParams);

            var totalItems = await _unitOfWork.Repository<Taste>().CountAsync(countSpec);

            var tastes = await _unitOfWork.Repository<Taste>().ListAsync(spec);

            return Ok(new Pagination<Taste>(specParams.PageIndex, specParams.PageSize, totalItems, tastes));
        }

        // GET api/<TastesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get([FromRoute] int id)
        {
            var taste =await _unitOfWork.Repository<Taste>().GetByIdAsync(id);

            if (taste == null)
            {
                return BadRequest();
            }

            return Ok(taste);
        }

        [HttpPost]
        public async Task<ActionResult<TasteDto>> Post([FromBody] TasteDto tasteDto)
        {
            var taste = _mapper.Map<Taste>(tasteDto);

            _unitOfWork.Repository<Taste>().Add<Taste>(taste);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem creating brand"));

            return tasteDto;
        }

        // PUT api/<TastesController>/5
        [HttpPut]
        public async Task<ActionResult<TasteDto>> Put([FromBody] TasteDto tasteDto)
        {
            var taste = _mapper.Map<Taste>(tasteDto);

            _unitOfWork.Repository<Taste>().Update(taste);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem updating taste"));

            return tasteDto;
        }

        // DELETE api/<TastesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var taste = await _unitOfWork.Repository<Taste>().GetByIdAsync(id);

            _unitOfWork.Repository<Taste>().Delete(taste);

            var result = await _unitOfWork.Complete();

            if (result <= 0) return BadRequest(new ApiResponse(400, "Problem deleting brand"));

            return Ok(new ApiResponse(200, "Brand was deleted succsessfuly!"));
        }

        [HttpGet("names/{name}")]
        public bool BrandNameExist(string name)
            => _unitOfWork
                .Repository<Taste>()
                .ListAllAsync()
                .Result
                .Any(n => n.Name == name);

    }
}
