import { Router, ActivatedRouteSnapshot, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BreadcrumbService } from 'xng-breadcrumb';

@Component({
  selector: 'app-section-header',
  templateUrl: './section-header.component.html',
  styleUrls: ['./section-header.component.scss']
})
export class SectionHeaderComponent implements OnInit {
  breadcrumb$: Observable<any[]>;

  currentRoute: string;

  constructor(
    private bcService: BreadcrumbService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.breadcrumb$ = this.bcService.breadcrumbs$;
    this.router.events.subscribe((url)=>{
      if(url instanceof NavigationEnd){
        this.currentRoute=url.url
        console.log(url.url)
      }
    })
  }

}
