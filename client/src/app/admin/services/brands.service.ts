import { IBrand} from 'src/app/shared/models/brand';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IPagination, Pagination } from 'src/app/shared/models/pagination';
import { map } from 'rxjs/operators';
import { BrandParams } from '../brands/brands-list/brandParams';

@Injectable({
  providedIn: 'root'
})
export class BrandsService {

  baseUrl = environment.apiUrl;

  brandParams=new BrandParams();

  pagination = new Pagination<IBrand>();

  constructor(private http: HttpClient) { }

  getById(id: number): Promise<any> {
    let brand = new Promise<any>((resolve, reject) => {
      this.http.get(this.baseUrl + `brands/${id}`)
        .toPromise()
        .then(
          res => {
            resolve(res);
          }
        )
        .catch((err) => {
          reject(err)
        })
    });
    return brand;
  }

  getBrands() {

    let params = new HttpParams();
    params = params.append('sort', this.brandParams.sort);
    params = params.append('pageIndex', this.brandParams.pageIndex.toString());
    params = params.append('pageSize', this.brandParams.pageSize.toString());

    return this.http.get<IPagination<IBrand>>(this.baseUrl + 'brands', { observe: 'response', params })
      .pipe(
        map(response => {
          this.pagination = response.body;
          return this.pagination;
        })
      )
  }

  setBrandParams(params: BrandParams) {
    this.brandParams = params;
  }

  getBrandParams(){
    return this.brandParams;
  }

  create(brand) {
    return this.http.post(this.baseUrl+'brands',brand);
  }

  update(brand) {
    return this.http.put(this.baseUrl+'brands',brand);
  }

  delete(id:number){
    return this.http.delete(this.baseUrl + 'brands/' + id);
  }

  isNameUnique(name: string) {
    return this.http.get<boolean>(this.baseUrl + `brands/names/${name}`);
  }
}
