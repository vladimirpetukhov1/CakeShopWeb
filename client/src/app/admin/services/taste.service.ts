import { ITaste } from 'src/app/shared/models/taste';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IPagination, Pagination } from 'src/app/shared/models/pagination';
import { map } from 'rxjs/operators';
import { TasteParams } from '../product-tastes/tasteParams';

@Injectable({
  providedIn: 'root'
})
export class TasteService {

  baseUrl = environment.apiUrl;

  pagination = new Pagination<ITaste>();
  tasteParams = new TasteParams();

  constructor(private http: HttpClient) { }

  getById(id: number): Promise<any> {
    let brand = new Promise<any>((resolve, reject) => {
      this.http.get(this.baseUrl + `tastes/${id}`)
        .toPromise()
        .then(
          res => {
            resolve(res);
          }
        )
        .catch((err) => {
          reject(err)
        })
    });
    return brand;
  }

  getTastes(id: number) {

    let params = new HttpParams();
    params = params.append('sort', this.tasteParams.sort);
    params = params.append('pageIndex', this.tasteParams.pageIndex.toString());
    params = params.append('pageSize', this.tasteParams.pageSize.toString());
    params = params.append('productId', id.toString());

    return this.http.get<IPagination<ITaste>>(this.baseUrl + `tastes`, { observe: 'response', params })
      .pipe(
        map(response => {
          this.pagination = response.body;
          return this.pagination;
        })
      )
  }

  create(taste) {
    return this.http.post(this.baseUrl + 'tastes', taste);
  }

  update(taste) {
    return this.http.put(this.baseUrl + 'tastes', taste);
  }

  delete(id: number) {
    return this.http.delete(this.baseUrl + 'tastes/' + id);
  }

  isNameUnique(name: string) {
    return this.http.get<boolean>(this.baseUrl + `tastes/names/${name}`);
  }

  setTypeParams(params: TasteParams) {
    this.tasteParams = params;
  }

  getTypeParams() {
    return this.tasteParams;
  }
}
