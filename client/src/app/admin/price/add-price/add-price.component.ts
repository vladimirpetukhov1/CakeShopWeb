import { MeassureService } from './../../services/meassure.service';
import { IMeassure } from './../../../shared/models/product';
import { Component, Input, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { IPrice, IProduct, ProductFormValues, IPriceToCreate } from 'src/app/shared/models/product';
import { ProductService } from '../../services/product.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-price',
  templateUrl: './add-price.component.html',
  styleUrls: ['./add-price.component.scss']
})
export class AddPriceComponent implements OnInit {

  @Input() product: ProductFormValues;
  price: IPrice;
  priceDialog: boolean;
  selectedPrices: IPrice[];
  progress = 0;
  submitted: boolean;

  meassures:IMeassure[];

  constructor(private productService: ProductService,
              public meassureService: MeassureService) { }

  ngOnInit(): void {
    this.priceDialog = false;

    this.meassureService.getAll().subscribe((data)=>{
      this.meassures=data;
    })
  }

  openNew() {
    this.submitted = false;
    this.priceDialog = true;
  }
  onProductChange(product: ProductFormValues) {
    this.product = product;
  }

  removePrice(id: number) { }

  editPrice(price:IPrice) { console.log(price); this.price=price;this.priceDialog=true;  }

  setAsMain(id: number) { }

  hideDialog() { this.priceDialog = false; }

  savePrice() { }

}
