import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IMeassure, IPrice, ProductFormValues } from 'src/app/shared/models/product';
import { MeassureService } from '../../services/meassure.service';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-edit-price-form',
  templateUrl: './edit-price-form.component.html',
  styleUrls: ['./edit-price-form.component.scss']
})
export class EditPriceFormComponent implements OnInit {

  @Input() product: ProductFormValues;
  @Output() productChange = new EventEmitter<ProductFormValues>();
  @Input() resetForm = new EventEmitter<boolean>();
  editPriceForm: FormGroup;
  @Input() meassures: IMeassure[];
  meassure: IMeassure;
  @Input() price: IPrice;

  selectedMeassure: IMeassure;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private meassureService: MeassureService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.meassureService.getAll()
      .subscribe((data) => {
        this.meassures = data;
        console.log(data);
      })
    this.createEditPriceForm();
  }

  createEditPriceForm() {
    this.editPriceForm = new FormGroup({
      id:new FormControl(),
      value: new FormControl(0, [
        Validators.required,
        Validators.min(0.01),
        this.regexValidator(
          new RegExp(
            '\\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(\\.[0-9][0-9])?$'
          ),
          { noDecimal: true }
        ),
      ]),
      quantity: new FormControl(null, Validators.required),
      meassureId: new FormControl()
    });
  }

  onSubmit() {
    // if (this.route.snapshot.url[0].path === 'edit') {
    //   const updatedProduct = {
    //     ...this.product,
    //     ...this.editPriceForm.value,
    //     value: +this.editPriceForm.get('value').value,
    //     quantity: +this.editPriceForm.get('quantity').value,
    //     meassureId: +this.editPriceForm.get('meassureId').value
    //   };
    this.editPriceForm.get('id').setValue(this.price.id);
    this.productService
      .addPrice(this.editPriceForm.value, +this.route.snapshot.paramMap.get('id'))
      .subscribe(
        (product) => {
          this.productChanges(product);
          this.reset();
        },
        (error) => console.log(error)
      );
    // } else {
    //   const newProduct = {
    //     ...this.editPriceForm.value,
    //     value: +this.editPriceForm.get('value').value,
    //     quantity: +this.editPriceForm.get('quantity').value,
    //     meassureId: +this.editPriceForm.get('meassureId').value
    //   };
    //   this.productService.createProduct(newProduct).subscribe(
    //     () => {
    //       this.router.navigate(['/admin']);
    //     },
    //     (error) => console.log(error)
    //   );
    // }
  }

  productChanges(product: any) {
    this.productChange.emit(product);
  }

  reset() { this.editPriceForm.reset(); this.resetForm.emit(true); }


  //TODO: make validation service
  regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: string } => {
      if (!control.value) {
        return null;
      }
      const valid = regex.test(control.value);
      return valid ? null : error;
    };
  }

}
