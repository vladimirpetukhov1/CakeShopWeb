import { MeassureService } from './../../services/meassure.service';
import { IMeassure } from './../../../shared/models/product';
import { Component, Input, OnInit } from '@angular/core';
import { IPrice, IProduct, ProductFormValues, IPriceToCreate } from 'src/app/shared/models/product';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-edit-price',
  templateUrl: './edit-price.component.html',
  styleUrls: ['./edit-price.component.scss']
})
export class EditPriceComponent implements OnInit {

  @Input() product: ProductFormValues;
  price: IPrice;
  priceDialog: boolean;
  selectedPrices: IPrice[];
  progress = 0;
  submitted: boolean;

  meassures: IMeassure[];

  constructor(private productService: ProductService,
    public meassureService: MeassureService) { }

  ngOnInit(): void {
    this.priceDialog = false;

    this.meassureService.getAll().subscribe((data) => {
      this.meassures = data;
    })
  }

  openNew() {
    if (this.price == undefined) {
      this.price = { quantity: 0, value: 0.00, meassureId: 1, isMain: false, id: null, meassure: null }
    }
    this.submitted = false;
    this.priceDialog = true;

  }
  onProductChange(product: ProductFormValues) {
    this.product = product;
  }

  removePrice(id: number) { this.productService.removePrice(this.product.id, id) }

  editPrice(price: IPrice) { this.price = price; this.priceDialog = true; }

  setAsMain(id: number) {
    this.productService.setMainPrice(id, this.product.id)
    .subscribe((product: ProductFormValues) => {
      this.product = product;
    })
  }

  hideDialog() { this.priceDialog = false; this.price = null; }

  savePrice() { }

}
