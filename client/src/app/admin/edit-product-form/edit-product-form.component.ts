import { IMeassure } from './../../shared/models/product';
import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { ProductFormValues, IProduct } from 'src/app/shared/models/product';
import { IBrand } from 'src/app/shared/models/brand';
import { IType } from 'src/app/shared/models/productType';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../services/product.service';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidationErrors,
  ValidatorFn,
  AbstractControl,
  FormArray,
} from '@angular/forms';
import { ITaste } from 'src/app/shared/models/taste';

@Component({
  selector: 'app-edit-product-form',
  templateUrl: './edit-product-form.component.html',
  styleUrls: ['./edit-product-form.component.scss'],
})
export class EditProductFormComponent implements OnInit {
  editProductForm: FormGroup;
  @Input() product: ProductFormValues;
  @Input() brands: IBrand[];
  @Input() types: IType[];
  @Input() meassures: IMeassure[];
  selectedTastes = [];


  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private router: Router,
    private chRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.createEditProductForm();
  }

  createEditProductForm() {
    this.editProductForm = new FormGroup({
      name: new FormControl('', Validators.required),
      // we need more validators
      price: new FormControl(0, [
        Validators.required,
        Validators.min(0.01),
        this.regexValidator(
          new RegExp(
            '\\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(\\.[0-9][0-9])?$'
          ),
          { noDecimal: true }
        ),
      ]),
      description: new FormControl(null, Validators.required),
      productBrandId: new FormControl(null, Validators.required),
      productTypeId: new FormControl(null, Validators.required),
      tastes: new FormControl(null)
    });
    this.editProductForm.patchValue(this.product);
    this.chRef.detectChanges();
  }

  onSubmit() {
    if (this.route.snapshot.url[1].path === 'edit') {
      const updatedProduct = {
        ...this.product,
        ...this.editProductForm.value,
        price: +this.editProductForm.get('price').value,
        productBrandId: +this.editProductForm.get('productBrandId').value,
        productTypeId: +this.editProductForm.get('productTypeId').value
      };

      this.productService
        .updateProduct(updatedProduct, +this.route.snapshot.paramMap.get('id'))
        .subscribe(
          (product: IProduct) => {
            this.editProductForm.patchValue(product);
          },
          (error) => console.log(error)
        );
    } else {
      const newProduct = {
        ...this.editProductForm.value,
        price: +this.editProductForm.get('price').value
      };
      this.productService.createProduct(newProduct).subscribe(
        (product: IProduct) => {
          this.router.navigate([`/admin/product/edit/${product.id}`]);
        },
        (error) => console.log(error)
      );
    }
  }

  addTaste(taste: ITaste) {
    console.log(this.product)

  }

  // Move this into a utils file
  regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: string } => {
      if (!control.value) {
        return null;
      }
      const valid = regex.test(control.value);
      return valid ? null : error;
    };
  }


}
