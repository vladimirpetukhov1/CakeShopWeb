import { TasteService } from './../services/taste.service';
import { TasteParams } from './tasteParams';
import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { IType } from 'src/app/shared/models/productType';
import { BrandTypeService } from '../services/brand-type.service';
import { ValidationService } from 'src/app/core/services/validation.service';
import { ITaste } from 'src/app/shared/models/taste';

@Component({
  selector: 'app-product-tastes',
  templateUrl: './product-tastes.html',
  styleUrls: ['./product-tastes.component.scss']
})
export class ProductTastesComponent implements OnInit {

  @Input() productId: number;
  @Input() tastes: ITaste[];
  typeForm: FormGroup;
  totalCount: number;
  pageSize = 0;
  pageNumber = 1;
  modalRef?: BsModalRef;
  brandType;
  typeParams = new TasteParams();
  modalTitle: string;
  sortSelected = 'name';
  taste: ITaste;
  count = 1;

  constructor(
    private tasteService: TasteService,
    private modalService: BsModalService,
    private fb: FormBuilder) {
    this.typeParams = this.tasteService.getTypeParams();
  }

  ngOnInit(): void {
    this.loadTastes();
    this.typeForm = this.fb.group({
      'name': new FormControl(null, {
        validators: [Validators.required]
      }),
      'description': new FormControl(null)
    });
  }

  getTypes() {
    this.loadTastes;
  }

  deleteType(id: number) {
    this.tasteService.delete(id).subscribe((res) => {
      this.loadTastes();
    })
  }

  loadTastes() {

  }

  onSubmit() {
    if (this.taste.id) {
      this.taste = {
        ...this.typeForm.value
      }
      this.tasteService.update(this.taste).subscribe((data) => {
        this.typeForm.reset();
        this.modalRef.hide();
        this.loadTastes();
      })
    } else {
      console.log(this.typeForm.value);
      this.taste = {
        ...this.typeForm.value
      }
      this.tasteService.create(this.taste).subscribe((data) => {
        this.typeForm.reset();
        this.modalRef.hide();
        this.loadTastes();
      })
    }
  }

  config = {
    keyboard: false,
  };
  //TODO: make modal to separate component
  openModal(brandTypeModal: TemplateRef<any>, title: string, id?: number) {
    this.taste = { name: null, description: null };
    this.modalTitle = title;
    if (id) {
      this.tasteService.getById(id).then(res => {
        this.taste = res
      })
    }
    this.modalRef = this.modalService.show(brandTypeModal, this.config);
  }

  onPageChanged($event: any) {
    const params = this.tasteService.getTypeParams();
    if (params.pageIndex !== $event) {
      params.pageIndex = $event;
      this.tasteService.setTypeParams(params);
      this.loadTastes();
    }
  }

}
