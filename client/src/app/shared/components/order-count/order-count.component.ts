import { IProduct } from './../../models/product';
import { BasketService } from './../../../basket/basket.service';
import { IPrice, IMeassure } from 'src/app/shared/models/product';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-count',
  templateUrl: './order-count.component.html',
  styleUrls: ['./order-count.component.scss']
})
export class OrderCountComponent implements OnInit {

  @Input() price: IPrice;
  @Input() meassures: IMeassure[];
  @Input() product: IProduct;

  count: number = 0;

  disabled = true;

  constructor(private basketService: BasketService) { }

  ngOnInit() {
  }

  addItemToBasket() {
    this.basketService.addItemToBasket(this.product, this.count);
  }

  decrementQuantity() {
    if (this.count <= 0) { } else { this.count--; }

  }

  incrementQuantity() {
    this.count++;
  }

}
