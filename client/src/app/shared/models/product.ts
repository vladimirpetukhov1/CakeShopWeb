import { IBrand } from './brand';
import { IType } from './productType';
import { ControlValueAccessor } from '@angular/forms';
import { ITaste } from './taste';

export interface IProduct {
  id: number;
  name: string;
  description: string;
  price: number;
  pictureUrl: string;
  productTypeId: number;
  productBrandId: number;
  photos: IPhoto[];
  prices: IPrice[];
}

export interface IPhoto {
  id: number;
  pictureUrl: string;
  fileName: string;
  isMain: boolean;
}

export interface IPrice {
  id: number;
  value: number;
  quantity: number;
  isMain: boolean;
  meassureId: number,
  meassure: IMeassure;
}

export interface IMeassure {
  id: number;
  name: string;
}

export interface IProductToCreate {
  id: number;
  name: string;
  description: string;
  price: number;
  pictureUrl: string;
  productTypeId: number;
  productBrandId: number;
}

export interface IPriceToCreate {
  quantity: number;
  value: number;
  meassureId: number;
  isMain: boolean;
}



export class ProductFormValues implements IProductToCreate {
  id: number;
  name = '';
  description = '';
  price = 0;
  pictureUrl = '';
  productBrandId: number;
  productTypeId: number;
  photos: IPhoto[];
  prices: IPrice[];

  constructor(init?: ProductFormValues) {
    Object.assign(this, init);
  }
}
