export class ShopParams {
  brandId = 0;
  typeId = 0;
  tasteId = 0;
  sort = 'name';
  pageNumber = 1;
  pageSize = 12;
  search: string;
}
