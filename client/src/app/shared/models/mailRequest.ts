export class MailRequest {
  toEmail:string;
  subject:string;
  body:string;
  attachments:File[];
}
