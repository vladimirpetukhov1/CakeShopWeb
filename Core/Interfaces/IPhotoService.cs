﻿using Core.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IPhotoService
    {
        Task<Photo> SaveToDiskAsync(IFormFile photo);
        Task<Photo> SaveToDatabaseAsync(IFormFile photo);
        void DeleteFromDatabase(Photo photo);
        void DeleteFromDisk(Photo photo);
    }
}
