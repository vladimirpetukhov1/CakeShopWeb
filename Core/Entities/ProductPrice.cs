namespace Core.Entities
{
    public class ProductPrice : BaseEntity
    {
        public decimal Value { get; set; }
        public int Quantity { get; set; }
        public bool IsMain { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int MeassureId { get; set; }
        public Meassure Meassure { get; set; }
    }
}