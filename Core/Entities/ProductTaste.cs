﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entities
{
    public class ProductTaste
    {
        public int ProductId { get; set; }
        public Product Product { get; set; }

        public int TasteId { get; set; }
        public Taste Taste { get; set; }
    }
}
