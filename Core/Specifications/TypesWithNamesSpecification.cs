﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Specifications
{
    public class TypesWithNamesSpecification:BaseSpecifcation<ProductType>
    {
        public TypesWithNamesSpecification(TypeSpecParams typeParams) : base()
        {
            ApplyPaging(typeParams.PageSize * (typeParams.PageIndex - 1), typeParams.PageSize);

            if (!string.IsNullOrEmpty(typeParams.Sort))
            {
                AddOrderBy(n => n.Name);
            }
        }
    }
}
