using Microsoft.EntityFrameworkCore;
using Core.Entities;

namespace Infrastructure.Data.Config
{
    public class ProductPriceConfiguration : IEntityTypeConfiguration<ProductPrice>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<ProductPrice> builder)
        {
            builder.Property(x => x.Quantity)
            .HasDefaultValue(0);
            builder.Property(x => x.IsMain)
            .HasDefaultValue(false);
        }
    }
}